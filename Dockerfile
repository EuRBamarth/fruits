FROM python:3.7
RUN  python --version

# Create app directory
WORKDIR /app

# copy requirements.txt
COPY requirements.txt ./


# Install app dependencies
RUN pip install -r requirements.txt

# Bundle app source
COPY src /app
